I followed [this tutorial](https://robertheaton.com/2018/06/12/programming-projects-for-advanced-beginners-ascii-art/), and this ascii art generator is what I made.  It looks like this:

![It looks like this](ppab01.png)

Oops, the terminal opacity makes it hard to see the image.
