#include <cstring>
#include <iostream>
#include <string>
#include <boost/program_options.hpp>
namespace args = boost::program_options;

#include <CImg.h>
namespace cimg = cimg_library;

void
print_img_uc_info(cimg::CImg<unsigned char> &Img)
{
	std::printf("%dx%d\nDepth: %d  Channels: %d\n",
			Img.width(), Img.height(), Img.depth(), Img.spectrum());
}

int
main(int Arg_c, char** Arg_v)
{
	// Init stuff
	int ImgWidth;
	std::string InFile_str;
	const std::string
		Usage_str("Usage: "+std::string(Arg_v[0])+" [options] input_file");
	args::options_description Usage(Usage_str);
	args::positional_options_description PosUsage;
	Usage.add_options()
		("help", "Show this help")

		("info", "Print image info.")

		("width",
		 args::value<int>(&ImgWidth)->default_value(80),
		 "Set maximum width of ascii image")

		("ifile",
		 args::value<std::string>(&InFile_str),
		 "Specify a file for input.")
		;
	PosUsage.add("ifile", -1);
	args::variables_map VarMap;
	try {
		args::store(args::command_line_parser(Arg_c, Arg_v)
				.options(Usage).positional(PosUsage).run(),
				VarMap);
	} catch (boost::wrapexcept<args::unknown_option>& E) {
		std::cout<< E.what()<< '\n';
		return 1;
	} catch (boost::wrapexcept<args::multiple_occurrences>& E) {
		std::cout<< "Must specify only one file as input.\n";
		return 1;
	} catch (boost::wrapexcept<args::invalid_command_line_syntax>& E) {
		std::cout<< E.what()<< '\n';
		return 1;
	}
	args::notify(VarMap);
	if (VarMap.count("help")) {
		std::cout<< Usage<< '\n';
		return 1;
	}
	if (VarMap.count("width")) {
		ImgWidth = VarMap["width"].as<int>();
	}
	if (VarMap.count("ifile")) {
		InFile_str = VarMap["ifile"].as<std::string>();
	} else {
		std::printf("Need a file for input\n");
		return 1;
	}

	cimg::CImg<unsigned char> Image;
	unsigned char* Image_data;
	try {
		Image.load(InFile_str.c_str());
	} catch (std::exception& E) {
		std::fprintf(stderr, "Exception @ %d: ", __LINE__);
		std::cerr<< E.what()<< '\n';
		return 1;
	}
	if (VarMap.count("info")) {
		print_img_uc_info(Image);
	}
	int W = Image.width(), H = Image.height();
	if (W > ImgWidth) {
		try {
			int NewW = ImgWidth, NewH;
			NewH = ((double) NewW/W)*H*0.5;
			Image.resize(NewW, NewH);
			assert((NewW == Image.width()) && (NewH == Image.height()));
		} catch (std::exception& E) {
			std::fprintf(stderr, "Exception @ %d: ", __LINE__);
			std::cerr<< E.what()<< '\n';
			return 1;
		}
	}

	constexpr char CharMatx[] =
		" `^\",:;Il!i~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$";
	try {
		for (int Y_idx = 0; Y_idx < Image.height(); ++Y_idx) {      // by rows
			for (int X_idx=0; X_idx < Image.width(); ++X_idx) {     // by columns
				float Avg = 0;
				if (Image.spectrum() > 1) {
					for (int Spec_idx = 0; Spec_idx < Image.spectrum(); ++Spec_idx)
						Avg += Image(X_idx, Y_idx, Spec_idx);
					Avg /= Image.spectrum(); // Average Channels
				} else {
					// One sample -- the sample IS the average.
					Avg = Image(X_idx, Y_idx, 0);
				}
				Avg /= 255;                  // Normalize Channels
				int Bright_idx = Avg * 65;
				std::cout<< CharMatx[Bright_idx];
			}
			std::cout<< '\n';
		}
	} catch (std::exception& E) {
		std::fprintf(stderr, "Exception @ %d: ", __LINE__);
		std::cerr<< E.what()<< '\n';
		return 1;
	}
	std::cout<< std::endl;

	// De-Init Stuff
	return 0;
}
